package Impl;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import Entities.Employe;
import Interface.EmployeServiceRemote;

@Stateless
public class EmployeService implements EmployeServiceRemote {
	@PersistenceContext(unitName = "imputation-ejb")
	EntityManager em;

	@Override
	public int ajouterEmploye(Employe employe) {
		em.persist(employe);
		return employe.getId();
	}
	@Override
	public Employe getEmployeByEmailAndPassword(String email, String password) {
	TypedQuery<Employe> query =
	em.createQuery("SELECT e FROM Employe e WHERE e.email=:email AND e.password=:password ",
	Employe.class);
	query.setParameter("email", email);
	query.setParameter("password", password);
	Employe employe = null;
	try { employe = query.getSingleResult(); }
	catch (Exception e) { System.out.println("Erreur : " + e); }
	return employe;
	}
}
