package Interface;

import javax.ejb.Remote;

import Entities.Employe;
@Remote
public interface EmployeServiceRemote {

	int ajouterEmploye(Employe employe);

	Employe getEmployeByEmailAndPassword(String email, String password);

}
