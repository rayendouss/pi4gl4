import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import Entities.Employe;
import Entities.Role;

import Interface.EmployeServiceRemote;


public class MainClient {

	public static void main(String[] args) throws NamingException {
		String jndiName = "TpJsf-ear/TpJsf-ejb/EmployeService!Interface.EmployeServiceRemote";
		Context context = new InitialContext();
		EmployeServiceRemote employeServiceremote = (EmployeServiceRemote) context.lookup(jndiName);
		Employe e = new  Employe("rayen","a","douss","ray",Role.ADMINISTRATEUR);
	
	//Role something = Role.ADMINISTRATEUR;
	
	
	employeServiceremote.ajouterEmploye(e);
	System.out.println(e);

	}

}
